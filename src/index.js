// Default SortableJS https://github.com/SortableJS/Sortable
import Sortable, { MultiDrag, Swap } from 'sortablejs';
Sortable.mount(new MultiDrag(), new Swap());

const sendBtn = document.getElementById('sendSortable');
const form = document.getElementById("sendSortable");
const xhr = new XMLHttpRequest();
const entity = document.querySelector('body').id.split('list-')[1];

if (document.querySelectorAll('.sortable').length > 1) {

	new Sortable(document.querySelector('tbody'), {
		group: 'table-order',
		handle: '.handle',
		multiDrag: false, // Enable multi-drag
		swap: true, // Enable swap plugin
		swapClass: 'highlight', // The class applied to the hovered swap item
		animation: 150,
		direction: 'vertical',
		onUpdate: function (evt) {
			if (form === null) {
				dataSendXhr(evt.item.dataset.id, evt.newIndex, evt.swapItem.dataset.id, evt.oldIndex)
			} else {
				initPosition(evt),
					dataUpdate(evt);
				activeButtonSend()
			}
		},
	});

	const sendData = {
		value: [],
		valueJson: '',
		itemCount: 0,
		add: function (item, index) {
			this.update(item.dataset.id);
			if (parseInt(item.dataset.postionInit) !== index) {
				sendData.value.push({ 'id': item.dataset.id, "sort": index });
			}
			this.itemCount = this.value.length;
			this.valueJson = JSON.stringify(sendData.value)
		},
		update: function (itemId) {
			this.value.forEach(function (el, i) {
				if (el.id === itemId) {
					this.value.splice(i, 1);
				}
			}, this);
		}
	};

	function dataUpdate(evt) {
		sendData.add(evt.item, evt.newIndex)
		sendData.add(evt.swapItem, evt.oldIndex)
	}

	function initPosition(evt) {
		if (evt.item.dataset.postionInit === undefined) {
			evt.item.dataset.postionInit = evt.oldIndex;
		}
		if (evt.swapItem.dataset.postionInit === undefined) {
			evt.swapItem.dataset.postionInit = evt.newIndex;
		}
	}

	function dataSendXhr(item1, order1, item2, order2) {

		let url = '/admin/?action=sortable&entity=' + entity
			+ "&item1=" + item1
			+ "&order1=" + order1
			+ "&item2=" + item2
			+ "&order2=" + order2;
		xhr.open('GET', url, 'async')
		xhr.send()
		xhr.onreadystatechange = function () {
			if (this.readyState === this.DONE) {
				console.error('request no found')
			}
		}
	}

	function activeButtonSend() {
		if (sendData.itemCount > 2) {
			return;
		} else if (sendBtn.style.opacity === '1') {
			sendBtn.style.opacity = '';
			sendBtn.style.zIndex = '';
		} else if (sendBtn.style.opacity === '') {
			sendBtn.style.opacity = 1;
			sendBtn.style.zIndex = 1;
		}
	}

	if (form !== null) {
		form.addEventListener("click", function (e) {
			document.getElementById("dataSortable").value = sendData.valueJson;
		}, false);
	}
}