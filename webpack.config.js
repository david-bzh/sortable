const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
global.faker = require('faker');

let config = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'public/assets/js'),
    filename: 'main.js',
  },
  devServer: {
    overlay: true,
    stats: {
        children: false,
        maxModules: 0
    },
    contentBase: path.resolve(__dirname, './public'), // in your specific case maybe?,
    hot: true,
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.ejs$/,
        use: ['ejs-loader']
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({'process.env': {NODE_ENV: JSON.stringify(process.env.NODE_ENV)}}),
    new webpack.ProgressPlugin(),
    new HtmlWebpackPlugin({template: './public/index.ejs'})
  ]
};

module.exports = config