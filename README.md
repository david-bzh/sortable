# Sortable

![Capture](/Capture.PNG)

### DevDependencies

- Babel/core
- Babel/plugin-syntax-dynamic-import
- Babel/preset-env
- Babel/babel-loader
- Babel/cross-env
- ejs-loader
- faker
- html-webpack-plugin
- Babel/webpack
- Babel/webpack-cli
- Babel/webpack-dev-server

### Dependencies
- sortablejs

## Getting Started

### Install with NPM:

  ```
    npm install
  ```